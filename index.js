// WDC028v1.5b-23 | JavaScript - Objects
// Graded Activity:

// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
// 	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
// 	(target.health - this.attack)

// 2.) If health is less than or equal to 0, invoke faint function


// */


function Pokemon(name, level, health){
	this.name = name
	this.level = level
	this.health = health * level
	this.attack = 10 * level



		this.tackle = function(target) {
		
			console.log(this.name + " tackled " + target.name);
			console.log(target.name + "'s health has been reduced to " + (target.health = target.health - this.attack));			
			

		
		},
		this.faint = function(target) {
			console.log(target.name + " fainted.")
		};
	
};


let balbasor = new Pokemon("Balbasor", 4, 20);
let pikachu = new Pokemon("Pikachu", 5, 20);

// console.log(balbasor.health);
console.log(balbasor);
balbasor.tackle(pikachu);
console.log(pikachu);
pikachu.tackle(balbasor);

console.log(balbasor);
balbasor.tackle(pikachu);
console.log(pikachu);
pikachu.tackle(balbasor);
console.log(balbasor);


if (pikachu.health <= 0 ) {
	pikachu.faint(pikachu);

}else if(balbasor.health <= 0){
	balbasor.faint(balbasor);
}












/*function Pokemon(name, level) {

	// Propeties
	this.name = name
	this.level = level
	this.health = 100 * level
	this.attack = 20 * level

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
	},

	this.faint = function() {
		console.log(this.name + " fainted.")
	}
};

let charmander = new Pokemon("Charmander", 12);
let squirtle = new Pokemon("Squirtle", 6);

console.log(charmander);
console.log(charmander);
console.log(charmander);
console.log(squirtle);
console.log(squirtle);*/